# text-based-apps

Text-based apps to run in the terminal through cURL, Telnet, SSH, ...

---

# Cheatsheet
`curl cheat.sh/:help`

# Cryptocurrency
`curl rate.sx`

# Dictionary
`curl dict.org/d:KEYWORD`

# Maps
`telnet mapscii.me`

# Link Shortener
`curl -F'shorten=https://example.com' https://0x0.st`

# QR Code
`curl qrenco.de`

# Weather
`curl wttr.in/:help`
